const router = require("express").Router();
let Record = require("../models/record.model");
let Request = require("../models/request.model");

// import for input validation
const { check, validationResult } = require("express-validator");

router.route("/filter").post(
  [
    // all values are basically checked for being a valid date and being a number
    check("startDate")
      .exists()
      .withMessage("Valid data must be provided")
      .isISO8601()
      .withMessage("Must be a valid date"),
    check("endDate")
      .exists()
      .withMessage("Valid data must be provided")
      .isISO8601()
      .withMessage("Must be a valid date"),
    check("minCount")
    .exists()
    .withMessage("Valid data must be provided")
    .isNumeric()
    .withMessage("Must be a numeric value"),
    check("maxCount")
    .exists()
    .withMessage("Valid data must be provided")
    .isNumeric()
    .withMessage("Must be a numeric value"),
  ],
  (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      // JSON validation errors are coded as 2
      return res.status(422).json({
        code: 2,
        msg: "invalid input data",
        errors: errors.array()
      });
    } else {
        // create the request object and get the results
      const startDate = Date.parse(req.body.startDate);
      const endDate = Date.parse(req.body.endDate);
      const minCount = Number(req.body.minCount);
      const maxCount = Number(req.body.maxCount);

      const newRequest = new Request({
        startDate,
        endDate,
        minCount,
        maxCount
      });
      // Aggregated query according to the specifications
      // first the records with range of dates are acquired
      // then countTotal is computed and second filter is applied according to totalCount range
      Record.aggregate([
        {
          $match: {
            createdAt: {
              $gt: newRequest.startDate,
              $lt: newRequest.endDate
            }
          }
        },
        {
          $project: {
            _id: 0,
            key: 1,
            createdAt: 1,
            countTotal: {
              $reduce: {
                input: "$counts",
                initialValue: 0,
                in: { $sum: ["$$value", "$$this"] }
              }
            }
          }
        },
        {
          $match: {
            countTotal: {
              $gt: newRequest.minCount,
              $lt: newRequest.maxCount
            }
          }
        }
      ])
        .then(records =>
          res.status(200).json({
            code: 0,
            msg: "Success",
            records: records
          })
        )
        .catch(err =>
          res.status(400).json({
            code: 1,
            msg: err
          })
        );
    }
  }
);

module.exports = router;
