const request = require("supertest");
const app = require("../app");
const mongoose = require("mongoose");
require("dotenv").config();
mongoose.connect(process.env.ATLAS_TEST_URI, {
  useNewUrlParser: true
});

describe("App test", () => {
  it("has a module", () => {
    expect(app).toBeDefined();
  });
  let server;

  beforeAll(() => {
    server = app.listen(3001);
  });

  it("can list the records with given valid input", async () => {
    request(server)
      .post("/records/filter")
      .send({
        startDate: "2016-01-26",
        endDate: "2018-02-02",
        minCount: 3400,
        maxCount: 5000
      })
      .expect(200);
  });
  it("fails if one of input field type is wrong", async () => {
    const res = await request(server)
      .post("/records/filter")
      .send({
        startDate: "2016-01-26",
        endDate: "2018-02-02",
        minCount: "what is wrong with me",
        maxCount: 5000
      });
    expect(res.body.code).toEqual(2);
  });
  it("fails if one of the input field is missing", async () => {
    const res = await request(server)
      .post("/records/filter")
      .send({
        startDate: "2016-01-26",
        endDate: "2018-02-02",
        maxCount: 5000
      });
    expect(res.body.code).toEqual(2);
  });

  afterAll(done => {
    mongoose.connection.close();
    server.close(done);
  });
});
