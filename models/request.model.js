const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const requestSchema = new Schema(
    {
        startDate: {type: Date, required: true},
        endDate: {type: Date, required: true},
        minCount: {type: Number, required: true,minimum: 0},
        maxCount: {type: Number, required: true, exclusiveMinimum: 0}
    },{
        timestamps: false
    }
);

const Request = mongoose.model("Exercise", requestSchema);

module.exports = Request;