// default imports
const express = require("express");
const cors = require("cors");

const app = express();

app.use(cors());
app.use(express.json());

const records = require('./routes/records');
app.use('/records', records);

module.exports = app;