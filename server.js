// default imports
const mongoose = require("mongoose");
const port = process.env.PORT || 5000;

require("dotenv").config();
// getting the connection srv for db
const uri = process.env.ATLAS_URI;
// connection to db
mongoose.connect(uri, {
  useNewUrlParser: true,
  useCreateIndex: true,
  useUnifiedTopology: true
});

const app = require("./app");

// listening for port
app.listen(port, () => {
    console.log(`Server is running on port: ${port}`);
  })
